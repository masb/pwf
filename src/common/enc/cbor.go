package enc

import (
    "encoding/hex"
    "fmt"
    "io"

    "github.com/fxamacker/cbor/v2"
)

/* ----- */

type RawCBOR = cbor.RawMessage
type CBOR RawCBOR
type SourceCBOR struct{ io.Reader }
type HexCBOR string

/* ----- */

func SinkCBOR(w io.Writer, arg interface{}) error {
    return cbor.NewEncoder(w).Encode(arg)
}

func ToCBOR(arg interface{}) (CBOR, error) {
    m, err := cbor.Marshal(arg)
    return m, err
}

func (c CBOR) Unmarshal(ret interface{}) error {
    return cbor.Unmarshal(c, ret)
}

func (c CBOR) Hex() HexCBOR {
    return HexCBOR(hex.EncodeToString(c))
}

func (c CBOR) String() string {
    var m map[interface{}]interface{}
    _ = c.Unmarshal(&m)
    return fmt.Sprintf("%v", m)
}

/* ----- */

func NewSourceCBOR(r io.Reader) SourceCBOR {
    return SourceCBOR{Reader: r}
}

func (c SourceCBOR) Unmarshal(ret interface{}) error {
    err := cbor.NewDecoder(c).Decode(ret)
    if err != nil && err != io.EOF {
        return err
    }
    return nil
}

/* ----- */

func (c HexCBOR) Unmarshal(ret interface{}) error {
    b, err := hex.DecodeString(string(c))
    if err != nil {
        return err
    }

    return CBOR(b).Unmarshal(ret)
}

func NewHexSourceCBOR(r io.Reader) SourceCBOR {
    return SourceCBOR{Reader: NewSourceCBOR(hex.NewDecoder(r))}
}
