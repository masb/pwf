package enc

import (
    "path"
    "text/template"
)

/* ----- */

const (
    RCExt      = ".rc"
    delimOpen  = "(."
    delimClose = ")"

    readSkip readMode = iota
    readInit          = iota
    readOk            = iota
)

/* ----- */

type RCConfigMap = map[string]interface{}

type RCConfig struct {
    dir      string
    lifo     []RCConfigMap
    readMode readMode
    readKey  string
}

type readMode int

/* ----- */

func (c *RCConfig) FuncMap() template.FuncMap {
    return template.FuncMap{
        "let": c.Let, "arr": c.Arr, "inc": c.Inc,
        "in": c.In, "out": c.Out,
        "read": c.Read, "eof": c.EOF,
    }
}

func (c *RCConfig) In(key string) interface{} {
    m := RCConfigMap{}
    c.lifo[0][key] = m
    c.lifo = append([]RCConfigMap{m}, c.lifo...)
    return nil
}

func (c *RCConfig) Out() interface{} {
    c.lifo = c.lifo[1:]
    return nil
}

func (c *RCConfig) Let(key string, arg interface{}) interface{} {
    c.lifo[0][key] = arg
    return nil
}

func (c *RCConfig) Arr(key string, argv ...interface{}) []interface{} {
    c.lifo[0][key] = argv
    return nil
}

func (c *RCConfig) Read(key string) interface{} {
    c.lifo[0][key] = ""
    c.readKey = key
    c.readMode = readInit
    return nil
}

func (c *RCConfig) Write(b []byte) (int, error) {
    switch c.readMode {
    case readInit:
        c.readMode++
    case readOk:
        arg := c.lifo[0][c.readKey].(string) + string(b)
        c.lifo[0][c.readKey] = arg
    }
    return 0, nil
}

func (c *RCConfig) EOF() interface{} {
    c.readMode = readSkip
    return nil
}

func (c *RCConfig) Inc(key, filename string) interface{} {
    rc, err := ParseFile(path.Join(c.dir, filename))
    if err == nil {
        c.lifo[0][key] = RawCBOR(rc)
    }
    return nil
}

/* ----- */

func ParseFile(filepath string) (CBOR, error) {
    t := &template.Template{}
    ret := RCConfig{
        dir:  path.Dir(filepath),
        lifo: []RCConfigMap{{}},
    }

    t.Delims(delimOpen, delimClose).Funcs(ret.FuncMap())
    _, err := t.ParseFiles(filepath)
    if err != nil {
        return nil, err
    }

    for _, t = range t.Templates() {
        err = t.Execute(&ret, nil)
        if err != nil {
            return nil, err
        }
    }

    return ToCBOR(ret.lifo[0])
}

func LoadFile(filepath string, ret interface{}) error {
    rc, err := ParseFile(filepath)
    if err != nil {
        return err
    }
    return rc.Unmarshal(ret)
}
