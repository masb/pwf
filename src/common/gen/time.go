package gen

import (
    "reflect"
    "time"
)

/* ----- */

type RandomTime time.Time

/* ----- */

func NewRandomTime(r *Rand) RandomTime {
    max := time.Date(2050, 1, 1, 0, 0, 0, 0, time.UTC).Unix()
    return RandomTime(time.Unix(r.Int63n(max), 0))
}

func (RandomTime) Generate(r *Rand, _ int) reflect.Value {
    return reflect.ValueOf(NewRandomTime(r))
}
