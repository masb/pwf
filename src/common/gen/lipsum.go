package gen

import (
    "reflect"
    "strings"
)

/* ----- */

type RandomLine string

type RandomWord string

/* ----- */

func NewRandomLine(r *Rand) RandomLine {
    return RandomLine(Lines[r.Intn(len(Lines))])
}

func (RandomLine) Generate(r *Rand, _ int) reflect.Value {
    return reflect.ValueOf(NewRandomLine(r))
}

/* ----- */

func NewRandomWord(r *Rand) RandomWord {
    w := strings.Split(Lines[r.Intn(len(Lines))], " ")
    return RandomWord(w[r.Intn(len(w))])
}

func (RandomWord) Generate(r *Rand, _ int) reflect.Value {
    return reflect.ValueOf(NewRandomWord(r))
}

/* ----- */

var Lines = [...]string{
    `Lorem ipsum dolor sit amet, consectetur adipiscing elit`,
    `Vestibulum eget arcu id orci consectetur luctus et non neque`,
    `Maecenas leo nunc, tristique nec lacus non, gravida pretium mauris`,
    `Nullam efficitur odio eget neque fermentum, vitae scelerisque sapien ultrices`,
    `Nulla mattis tempor enim ac laoreet`,
    `Vivamus posuere lectus ac ornare vulputate`,
    `Aliquam ut orci non libero facilisis pulvinar eu sit amet leo`,
    `Morbi accumsan mollis libero, varius volutpat leo egestas non`,
    `Maecenas sed mauris sed est pretium porta`,
    `Nulla semper, felis at cursus dapibus, augue dolor lacinia turpis, dapibus auctor libero ante quis est`,
    `Vestibulum eu justo in enim vestibulum iaculis ac vitae ipsum`,
    `Nam non mi id quam commodo ultrices vitae in nunc`,
    `Vivamus nibh mauris, aliquam eget ultricies vitae, convallis nec dui`,
    `Pellentesque ornare pharetra mi, eu volutpat quam`,
    `Donec varius auctor tortor, sed fringilla dolor mollis nec`,
    `Mauris pulvinar imperdiet nulla eu laoreet`,
    `Pellentesque a odio vehicula, semper metus ut, facilisis metus`,
    `Fusce porta nibh et sapien congue, vel interdum enim pretium`,
    `Duis fermentum et neque sit amet eleifend`,
    `Sed id consequat diam, vel pellentesque quam`,
    `Integer interdum, ligula vel pretium auctor, nisi tellus condimentum magna, et consectetur arcu ipsum a metus`,
    `Cras nec felis nisl`,
    `Pellentesque a sapien sed mauris porta viverra`,
    `Cras in diam aliquam nunc ornare consectetur at quis lectus`,
    `Proin consequat eros eu commodo laoreet`,
    `Etiam interdum est vel faucibus posuere`,
    `Curabitur ipsum lacus, placerat sit amet dolor ac, sagittis pretium justo`,
    `Ut sapien libero, volutpat non condimentum non, volutpat eu purus`,
    `Etiam lacinia enim eu mi lobortis, vel laoreet magna pellentesque`,
    `In hac habitasse platea dictumst`,
    `Aliquam varius, enim sit amet sollicitudin ultrices, ante leo mollis ipsum, eu aliquet ante leo eget massa`,
    `Nunc quis pulvinar dolor`,
    `Mauris faucibus consectetur orci a vulputate`,
    `In molestie in magna convallis vehicula`,
    `Sed ac odio ut ex commodo bibendum et id sapien`,
    `Donec imperdiet hendrerit risus, et molestie quam vestibulum eu`,
    `Proin a enim quis turpis laoreet porttitor a in purus`,
    `Morbi a ipsum sagittis, vestibulum est nec, facilisis nisl`,
    `Nulla quis ipsum eget velit venenatis sollicitudin consectetur a leo`,
    `Nulla quis metus eu felis lacinia pharetra`,
    `Morbi fermentum pellentesque nulla`,
    `Vivamus sed augue venenatis, feugiat nunc imperdiet, elementum orci`,
    `Mauris hendrerit convallis metus et ultricies`,
    `Pellentesque sit amet est gravida lorem egestas eleifend`,
    `Praesent posuere ut nisi eu sagittis`,
    `In leo odio, cursus sed pellentesque a, accumsan sed ante`,
    `Nullam quis tristique arcu`,
    `Proin non nisi id mauris condimentum lacinia`,
    `Nullam id mi ac sem tincidunt cursus vel ac leo`,
    `Vivamus metus mauris, aliquam in vehicula sit amet, vulputate ac arcu`,
    `Vestibulum varius elit felis, porta molestie dui pulvinar sed`,
    `Nullam sapien risus, maximus nec dapibus vitae, lacinia sit amet ante`,
    `Donec id justo pellentesque, facilisis dui eget, congue nisi`,
    `Nunc et urna luctus, pellentesque turpis vel, bibendum neque`,
    `Suspendisse tincidunt elementum convallis`,
    `Aenean tristique augue ac ante tristique scelerisque`,
    `Phasellus et pellentesque felis`,
    `Suspendisse id elit dignissim, venenatis ipsum non, pretium augue`,
    `Quisque ullamcorper ullamcorper semper`,
    `Duis in erat tortor`,
    `Phasellus non dolor non massa dapibus venenatis`,
}
