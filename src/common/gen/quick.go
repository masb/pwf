package gen

import (
    "math"
    "math/rand"
    "reflect"
    "testing/quick"
    "time"
)

/* ----- */

type Rand = rand.Rand

/* ----- */

func Value(arg interface{}, r *Rand) interface{} {
    ret, _ := quick.Value(reflect.TypeOf(arg), r)
    return ret.Interface()
}

func Int8Config() *quick.Config {
    return &quick.Config{MaxCount: rand.Intn(math.MaxInt8)}
}

/* ----- */

func NewRand() *Rand {
    return (*Rand)(rand.New(rand.NewSource(time.Now().UnixNano())))
}
