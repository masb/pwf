package sugar

import (
    "reflect"
)

/* ----- */

func discover(t reflect.Type, rcv interface{}, argv ...interface{}) *ChanIter {
    v := []reflect.Value{reflect.ValueOf(rcv)}
    for _, i := range argv {
        v = append(v, reflect.ValueOf(i))
    }
    n, lenv := t.NumMethod(), len(v)
    ret := ChanIter{C: make(chan error, n)}
    go func() {
        for i := 0; i < n; i++ {
            ret.M.Lock()
            m := t.Method(i)
            if m.Type.NumIn() == lenv {
                retv := m.Func.Call(v[:lenv])
                if len(retv) != 0 {
                    switch err := retv[0].Interface().(type) {
                    case nil:
                        ret.C <- nil
                        continue
                    case error:
                        ret.C <- err
                        continue
                    }
                }
            }
            ret.Next()
        }
        close(ret.C)
    }()
    return &ret
}

func Discover(rcv interface{}, argv ...interface{}) *ChanIter {
    return discover(reflect.TypeOf(rcv), rcv, argv...)
}

func DiscoverType(t reflect.Type, argv ...interface{}) *ChanIter {
    return discover(t, reflect.Zero(t), argv...)
}
