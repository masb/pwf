package attar

import (
    "net/http"
    "path"

    "pwf/src/common/enc"
)

/* ----- */

type Handler interface {
    Init(enc.CBOR) (HandlerPath, error)
    Serve(http.ResponseWriter, *http.Request)
}

type HandlerPath struct {
    Path string
    Fn   http.HandlerFunc

    Disabled, ExactMatch bool
    // Auth bool
}

/* ----- */

func NewServer(argv []string, handlerSet ...Handler) (*http.Server, error) {
    filepath := path.Base(argv[0]) + enc.RCExt
    if len(argv) > 1 {
        filepath = path.Join(argv[1], filepath)
    }
    rc, err := enc.ParseFile(filepath)
    if err != nil {
        return nil, err
    }

    srv := struct {
        ServerAddr string
        ServerLang LangHandler
    }{}

    err = rc.Unmarshal(&srv)
    if err != nil {
        return nil, err
    }

    ret, mux := http.Server{Addr: srv.ServerAddr}, http.NewServeMux()
    for _, i := range handlerSet {
        hp, err := i.Init(rc)
        if err != nil {
            return nil, err
        }

        hp.Fn = i.Serve
        mux.HandleFunc(hp.Path, hp.Handle)
    }
    ret.Handler = mux

    if srv.ServerLang != nil {
        srv.ServerLang.Wrap(&ret)
    }
    return &ret, err
}

func ServerFailure(err error) bool {
    return err != nil && err != http.ErrServerClosed
}

/* ----- */

func (hp *HandlerPath) Handle(w http.ResponseWriter, r *http.Request) {
    if !hp.Disabled {
        if hp.ExactMatch && r.URL.Path != hp.Path {
            http.NotFound(w, r)
            return
        }
        hp.Fn(w, r)
    }
}

/* ----- */

func PathAsRoot(ep string) string {
    ep = path.Join("/", ep, "x")
    return ep[:len(ep)-1]
}
