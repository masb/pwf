package attar

import (
    "fmt"
    "mime"
    "net/http"
    "os"
    "path"
    "syscall"
)

/* ----- */

func ContentType(w http.ResponseWriter, str string, utf8 bool) {
    if utf8 {
        str += "; charset=utf-8"
    }
    w.Header().Set("Content-Type", str)
}

func ContentTypeAuto(w http.ResponseWriter, filepath string) {
    str := mime.TypeByExtension(path.Ext(filepath))
    err := syscall.Access(filepath, 1)
    ContentType(w, str, err != nil && !os.IsNotExist(err))
}

func InternalError(w http.ResponseWriter, str string) {
    code := http.StatusInternalServerError
    status := http.StatusText(code)
    http.Error(w, status, code)
    fmt.Printf("%s: %s\n", status, str)
}

func Error(w http.ResponseWriter, code int, str string) {
    http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(code), str), code)
}

func NotFoundError(w http.ResponseWriter, str string) {
    Error(w, http.StatusNotFound, str)
}

func InvalidMethod(w http.ResponseWriter, str string) {
    Error(w, http.StatusMethodNotAllowed, str)
}
