package attar

import (
    "net/http"
    "path"
    "text/template"

    "pwf/src/common/enc"
)

/* ----- */

type WasmHandler struct {
    Endpoint string `cbor:"Wasm"`
    StaticEP string `cbor:"Static"`
}

/* ----- */

func (h *WasmHandler) Init(rc enc.CBOR) (HandlerPath, error) {
    wasm := struct{ EndpointMap *WasmHandler }{h}
    err := rc.Unmarshal(&wasm)
    return HandlerPath{Path: PathAsRoot(h.Endpoint)}, err
}

func (h *WasmHandler) Serve(w http.ResponseWriter, r *http.Request) {
    // if r.ProtoMajor != 2 {
    //     w.WriteHeader(http.StatusInternalServerError)
    //     return
    // }

    // get wasm binary name
    ContentType(w, "application/xhtml+xml", true)

    t, lang := template.Template{}, w.Header().Get(ContentLanguageKey)
    t.Funcs(template.FuncMap{
        "lang":   func() string { return lang },
        "static": func() string { return path.Join("/", lang, h.StaticEP) },
    })

    _, err := t.Parse(xhtml)
    if err != nil {
        panic(err)
    }

    err = t.Execute(w, nil)
    if err != nil {
        panic(err)
    }
}

/* ----- */

const xhtml = `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{lang}}" xml:lang="{{lang}}">
    <head>
        {{with $static := static}}
            <link rel="shortcut icon" type="image/x-icon" href="{{$static}}/favicon.ico" />
            <title>Go WASM loader</title>
            <script src="{{$static}}/wasm_exec.js"></script>
            <script>
                const go = new Go();
                WebAssembly.instantiateStreaming(fetch("{{$static}}/main.wasm"), go.importObject).then((result) => {
                    go.run(result.instance);
                });
            </script>
        {{end}}
    </head>
    <body></body>
</html>
`
