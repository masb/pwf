package attar

import (
    "pwf/src/common/sugar"
)

/* ----- */

type CRUD interface {
    CreateQuerier() Querier
    ReadQuerier() Querier
    UpdateQuerier() Querier
    DeleteQuerier() Querier
}

type Querier interface {
    Bind(string, interface{})
    Query() error
    ReadOne(...interface{}) error
    ReadMany(...interface{}) *sugar.ChanIter
}
