package attar

import (
    "database/sql"

    // SQLite driver used trough database/sql
    _ "github.com/mattn/go-sqlite3"

    "pwf/src/common/sugar"
)

/* ----- */

const SQLiteMemoryDSN = ":memory:"

/* ----- */

type SQLiteDB struct{ *sql.DB }

type SQLiteQuerier struct {
    *SQLiteDB

    Binded   map[string]interface{}
    QueryStr string
    Cur      *sql.Rows
}

/* ----- */

func NewSQLiteDB(dsn string) (*SQLiteDB, error) {
    db, err := sql.Open("sqlite3", dsn)
    return &SQLiteDB{db}, err
}

func (db *SQLiteDB) CreateQuerier() Querier {
    return nil
}

func (db *SQLiteDB) ReadQuerier() Querier {
    return nil
}

func (db *SQLiteDB) UpdateQuerier() Querier {
    return nil
}

func (db *SQLiteDB) DeleteQuerier() Querier {
    return nil
}

/* ----- */

func NewSQLiteQuerier(db *SQLiteDB, str string) *SQLiteQuerier {
    return &SQLiteQuerier{
        SQLiteDB: db,
        Binded:   map[string]interface{}{},
        QueryStr: str,
    }
}

func (q *SQLiteQuerier) Bind(key string, arg interface{}) {
    q.Binded[key] = arg
}

func (q *SQLiteQuerier) Query() error {
    argv := []interface{}{}
    for k, v := range q.Binded {
        argv = append(argv, sql.Named(k, v))
    }

    var err error
    q.Cur, err = q.SQLiteDB.Query(q.QueryStr, argv...)
    return err
}

func (q *SQLiteQuerier) ReadOne(argv ...interface{}) error {
    defer q.Cur.Close()
    if !q.Cur.Next() {
        err := q.Cur.Err()
        if err != nil {
            return err
        }
        return sql.ErrNoRows
    }
    err := q.Cur.Scan(argv...)
    if err != nil {
        return err
    }
    return q.Cur.Close()
}

func (q *SQLiteQuerier) ReadMany(argv ...interface{}) *sugar.ChanIter {
    return nil
}
