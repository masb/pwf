package attar

import (
    "net/http"
    "os"
    "strings"

    "pwf/src/common/enc"
)

/* ----- */

type StaticHandler struct {
    http.Dir

    Endpoint string
}

/* ----- */

func (h *StaticHandler) Init(rc enc.CBOR) (HandlerPath, error) {
    type tmp struct{ Static *string }
    static := struct {
        Dir         *http.Dir `cbor:"StaticRootDirectory"`
        EndpointMap tmp
    }{&h.Dir, tmp{&h.Endpoint}}

    err := rc.Unmarshal(&static)
    return HandlerPath{Path: PathAsRoot(h.Endpoint)}, err
}

func (h *StaticHandler) Serve(w http.ResponseWriter, r *http.Request) {
    r.URL.Path = strings.TrimPrefix(r.URL.Path, h.Endpoint)
    err := h.CatFile(w, r.URL.Path)
    if err != nil {
        if os.IsNotExist(err) {
            NotFoundError(w, r.URL.Path)
        } else {
            InternalError(w, err.Error())
        }
    }
}

func (h *StaticHandler) CatFile(w http.ResponseWriter, path string) error {
    f, err := h.Open(path)
    if err != nil {
        return err
    }

    stat, err := f.Stat()
    if err != nil {
        return err
    }

    if !stat.Mode().IsRegular() {
        return os.ErrNotExist
    }

    ContentTypeAuto(w, string(h.Dir)+path)
    return CatFile(w, f)
}
