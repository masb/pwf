package attar

import (
    "net/http"
    "path"
    "strings"

    "golang.org/x/text/language"
)

/* ----- */

const ContentLanguageKey = "Content-Language"

/* ----- */

type LangHandler []string

/* ----- */

func (h LangHandler) Wrap(ret *http.Server) {
    sub, mux := ret.Handler, http.NewServeMux()
    mux.HandleFunc("/", h.ServeRoot)
    for _, i := range h {
        dup := "/" + i
        mux.HandleFunc(dup+"/", func(w http.ResponseWriter, r *http.Request) {
            r.URL.Path = strings.TrimPrefix(r.URL.Path, dup)
            w.Header().Set(ContentLanguageKey, dup[1:])
            sub.ServeHTTP(w, r)
        })
    }
    ret.Handler = mux
}

func (h LangHandler) ServeRoot(w http.ResponseWriter, r *http.Request) {
    tagMatcher := make([]language.Tag, len(h))
    for n, i := range h {
        tagMatcher[n] = language.Make(i)
    }
    matcher := language.NewMatcher(tagMatcher)
    cookie, _ := r.Cookie("lang")
    accept := r.Header.Get("Accept-Language")
    tag, _ := language.MatchStrings(matcher, cookie.String(), accept)
    lang, _ := tag.Base()
    url := *r.URL
    url.Path = path.Join("/"+lang.String(), url.Path)
    http.Redirect(w, r, url.String(), http.StatusTemporaryRedirect)
}
