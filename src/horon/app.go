package horon

import ()

/* ----- */

type App struct {
    renderCb   Callback
    popStateCb Callback

    RouteMap map[string]Component
}

/* ----- */

func (app *App) Render(c Component) (Element, bool) {
    switch e := c.Get(); e {
    case nil:
        if ci, ok := c.(ComponentWithInit); ok {
            ci.OnInit()
        }
        return c.Set(c.View()), true
    default:
        ret := c.Set(c.View())
        if ret.HashValue() != e.HashValue() {
            e.Free()
            return ret, true
        }
        return ret, false
    }
}

func (app *App) Mount(root Element, c Component) {
    v := root.JSValue()
    renderCb := func() {
        if e, ok := app.Render(c); ok {
            v.Set(ElementTextContentKey, "")
            v.Call(ElementAppendChildKey, e.JSValue())
        }
    }
    app.renderCb = NewWrappedCallback(renderCb)
    app.renderCb.ListenAll(v)
    renderCb()
}

func (app *App) Route(root Element, route string) {
    if app.RouteMap != nil {
        route = "#" + route

        popStateCb := func() {
            hash, state := PopLocationHash(), HistoryStateHash()
            if hash == "" {
                hash = state
            } else if hash == state {
                return
            }

            if c, ok := app.RouteMap[hash[1:]]; ok {
                c.Set(nil)
                app.Mount(root, c)
                PushHistoryStateHash(hash)
            }
        }

        app.popStateCb = NewWrappedCallback(popStateCb)
        Window().Set(OnPopStateKey, app.popStateCb)
        PushLocationHash(route)
    }
}

func (app *App) Wait() {
    var cb Callback
    cb = NewWrappedCallback(func() {
        app.popStateCb.Release()
        app.renderCb.Release()
        cb.Release()
        println("App memory freed. Bye!")
    })
    Window().Set(BeforeunloadKey, cb)
    select {}
}
