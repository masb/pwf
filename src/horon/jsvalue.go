package horon

import (
    "syscall/js"
)

/* ----- */

const (
    BeforeunloadKey = "beforeunload"

    LocationKey       = "location"
    LocationPathKey   = "pathname"
    LocationHashKey   = "hash"
    LocationSearchKey = "search"

    HistoryKey        = "history"
    HistoryPushKey    = "pushState"
    HistoryReplaceKey = "replaceState"
    HistoryStateKey   = "state"

    DocumentKey        = "document"
    DocumentElementKey = "documentElement"
    DocumentLangKey    = "lang"
    TitleKey           = "title"
    BodyKey            = "body"

    CreateElementKey       = "createElement"
    ElementTagKey          = "tagName"
    ElementTextKey         = "innerText"
    ElementSetAttributeKey = "setAttribute"
    ElementAppendChildKey  = "appendChild"
    ElementTextContentKey  = "textContent"

    OnClickKey      = "onclick"
    OnHashChangeKey = "onhashchange"
    OnPopStateKey   = "onpopstate"
)

/* ----- */

type Value = js.Value

/* ----- */

func Window() Value {
    return js.Global()
}

func Document() Value {
    return Window().Get(DocumentKey)
}

func DocumentLang() string {
    return Document().Get(DocumentElementKey).Get(DocumentLangKey).String()
}

func LocationPath() string {
    return Window().Get(LocationKey).Get(LocationPathKey).String()
}

func HistoryStateHash() string {
    return Window().Get(HistoryKey).Get(HistoryStateKey).String()
}

func PushHistoryStateHash(state string) {
    win := Window()
    h, d := win.Get(HistoryKey), win.Get(DocumentKey)
    h.Call(HistoryReplaceKey, state, d.Get(TitleKey))
}

func PopLocationHash() string {
    win := Window()
    l, h, d := win.Get(LocationKey), win.Get(HistoryKey), win.Get(DocumentKey)
    ret := l.Get(LocationHashKey).String()
    if ret != "" {
        state := h.Get(HistoryStateKey).String()
        url := l.Get(LocationPathKey).String() + l.Get(LocationSearchKey).String()
        h.Call(HistoryReplaceKey, state, d.Get(TitleKey), url)
    }
    return ret
}

func PushLocationHash(hash string) {
    Window().Get(LocationKey).Set(LocationHashKey, hash)
}
