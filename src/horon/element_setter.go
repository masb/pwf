package horon

import (
    "bytes"
    "fmt"
    "hash/fnv"
    "os"
)

/* ----- */

type AttributeSetter struct {
    Key, Value string
}

type CallbackSetter struct {
    Key string
    Cb  Callback
}

type ElementSetter struct {
    Tag, Text string
    AttrSet   []*AttributeSetter
    CbSet     []*CallbackSetter
    Children  []Element
}

/* ----- */

func (e *ElementSetter) Hash() uint32 {
    var b bytes.Buffer
    fmt.Fprintf(&b, "%s %s\n", e.Tag, e.Text)
    for _, i := range e.AttrSet {
        fmt.Fprintf(&b, "%v ", *i)
    }
    for _, i := range e.CbSet {
        fmt.Fprintf(&b, "%s ", i.Key)
    }
    for _, i := range e.Children {
        fmt.Fprintf(&b, "%x ", i.HashValue())
    }
    ret := fnv.New32a()
    _, err := ret.Write(b.Bytes())
    if err != nil {
        fmt.Fprintln(os.Stderr, err)
    }
    return ret.Sum32()
}

func (e *ElementSetter) Element() Element {
    ret := DOMElement{CbSet: e.CbSet}
    ret.Value = Document().Call(CreateElementKey, e.Tag)
    ret.Set(ElementTextKey, e.Text)
    for _, i := range e.AttrSet {
        ret.Call(ElementSetAttributeKey, i.Key, i.Value)
    }
    for _, i := range e.CbSet {
        ret.Set(i.Key, i.Cb)
    }
    for _, i := range e.Children {
        ret.Call(ElementAppendChildKey, i.JSValue())
    }
    ret.Hash = e.Hash()
    return &ret
}
