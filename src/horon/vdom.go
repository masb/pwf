package horon

import ()

/* ----- */

type Attribute interface {
    Key() string
    Value() string
}

type Element interface {
    JSValue() Value
    HashValue() uint32
    Free()

    Tag() string
    Text() string
    AttrSet() []Attribute
    CbKeySet() []string
    Children() []Element
}

type Children = []Element

/* ----- */

func Body() Element {
    return &DOMElement{Value: Document().Get(BodyKey)}
}

func E(tag string, content ...interface{}) Element {
    ret := ElementSetter{Tag: tag}
    for _, i := range content {
        switch arg := i.(type) {
        case string:
            ret.Text = arg
        case *AttributeSetter:
            ret.AttrSet = append(ret.AttrSet, arg)
        case *CallbackSetter:
            ret.CbSet = append(ret.CbSet, arg)
        case Children:
            ret.Children = append(ret.Children, arg...)
        }
    }
    return ret.Element()
}

func Class(selector string) *AttributeSetter {
    return &AttributeSetter{Key: "class", Value: selector}
}

func Href(url string) *AttributeSetter {
    return &AttributeSetter{Key: "href", Value: url}
}

func If(cond bool, e Element) Element {
    if cond {
        return e
    }
    return nil
}

func OnClick(fn CallbackFunc) *CallbackSetter {
    return &CallbackSetter{Key: OnClickKey, Cb: NewCallback(fn)}
}

func OnClickWrap(fn func()) *CallbackSetter {
    return &CallbackSetter{Key: OnClickKey, Cb: NewWrappedCallback(fn)}
}
