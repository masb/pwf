# PWF - Palea Web Frameworks

Backend and frontend frameworks for Palea organisation websites

### Try it

##### Build

You need first the golang linters:

```sh
$ go get -mod=readonly github.com/mbenkmann/goformat/goformat
$ go get -mod=readonly github.com/golangci/golangci-lint/cmd/golangci-lint
$ export PATH="$PATH:$HOME/go/bin"
```

> NOTE: this is the fast and dirty way to install them but we're here to try

Then build it with the tup build system:

```sh
$ tup init           # run this command first to parse the tup files
$ tup                # then this one to build if your system supports fuse
$ tup generate >(sh) # or this one otherwise
```

> NOTE: replace `>(sh)` to any filename to save the script

##### Create and populate the public root directory

```sh
$ mkdir www
$ # get the golang wasm loader script
$ ln -srv $(go env GOROOT)/misc/wasm/wasm_exec.js www/
$ ln -srv build/demo/hello_world/main.wasm www/
```

##### Run the server

```sh
$ ./build/demo/hello_world/server demo/hello_world
```

##### Try another demo

```sh
$ ln -srfv build/demo/routing/main.wasm www/
$ ./build/demo/routing/server demo/routing
```

You can now visit the website: http://localhost:8080/le-chocolat-blanc
