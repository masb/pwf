package main

import (
    "os"

    "pwf/src/attar"
)

/* ----- */

func main() {
    handlerSet := []attar.Handler{
        &attar.WasmHandler{}, &attar.StaticHandler{},
    }
    srv, err := attar.NewServer(os.Args, handlerSet...)
    if err != nil {
        panic(err)
    }

    err = srv.ListenAndServe()
    if attar.ServerFailure(err) {
        panic(err)
    }
}
