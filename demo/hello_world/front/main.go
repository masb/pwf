package main

import (
    "fmt"

    h "pwf/src/horon"
)

/* ----- */

type Hello struct {
    h.ComponentCache

    Count int
}

/* ----- */

func (c *Hello) OnInit() {
    c.Count = 0
}

func (c *Hello) onButtonClick() {
    c.Count++
}

func (c *Hello) View() h.Element {
    return h.E("main", h.Children{
        h.E("h1", h.Class("title"), "My first app"),
        h.E("button", h.OnClickWrap(c.onButtonClick), fmt.Sprintf("%d clicks", c.Count)),
    })
}

func main() {
    app := h.App{}
    app.Mount(h.Body(), &Hello{})
    app.Wait()
}
