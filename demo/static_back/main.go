package main

import (
    "os"

    "pwf/src/attar"
)

/* ----- */

func main() {
    srv, err := attar.NewServer(os.Args, &staticHandler{})
    if err != nil {
        panic(err)
    }

    err = srv.ListenAndServe()
    if attar.ServerFailure(err) {
        panic(err)
    }
}
