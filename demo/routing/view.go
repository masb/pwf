package pl

import (
    "pwf/src/common/enc"
)

/* ----- */

type ViewIDPayload struct {
    ID string
}

type ViewPayload struct {
    Route string
    State enc.RawCBOR
}
