package main

import (
    "fmt"

    "pwf/src/common/enc"
    h "pwf/src/horon"

    "pwf/demo/routing"
)

/* ----- */

type Hello struct {
    h.ComponentCache

    Count int
}

type Splash struct {
    h.ComponentCache
}

/* ----- */

func (c *Hello) OnInit() {
    c.Count = 0
}

func (c *Hello) onButtonClick() {
    c.Count++
}

func (c *Hello) View() h.Element {
    return h.E("main", h.Children{
        h.E("h1", h.Class("title"), "My first app"),
        h.E("button", h.OnClickWrap(c.onButtonClick), fmt.Sprintf("%d clicks", c.Count)),
    })
}

func (c *Splash) View() h.Element {
    return h.E("a", h.Href("#/hello"), "Enter!")
}

func main() {
    vid := pl.ViewIDPayload{ID: h.CurrentPath()[1:]}
    req := h.RequestOpt{Method: "READ", URL: h.HostPath("/view"), Body: &vid}
    resp, err := h.Request(&req)
    if err != nil {
        panic(err)
    }

    var view pl.ViewPayload
    err = enc.NewSourceCBOR(resp.Body).Unmarshal(&view)
    if err != nil {
        panic(err)
    }

    fmt.Printf(">>> Route:%q, State:%#v\n", view.Route, enc.CBOR(view.State).String())

    app := h.App{RouteMap: map[string]h.Component{
        "/splash": &Splash{},
        "/hello":  &Hello{},
    }}
    app.Route(h.Body(), view.Route)
    app.Wait()
}
