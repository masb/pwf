package main

import (
    "net/http"
    "reflect"

    "pwf/src/attar"
    "pwf/src/common/enc"

    "pwf/demo/routing"
)

/* ----- */

const publicMethodPrefix = "Public"

/* ----- */

type ViewHandler struct {
    attar.CRUD

    Endpoint string `cbor:"View"`
}

/* ----- */

func initDB(rc enc.CBOR) (*attar.SQLiteDB, error) {
    type View struct{ CreateQuery, PopulateQuery string }
    view := struct {
        View `cbor:"View"`
    }{}

    err := rc.Unmarshal(&view)
    if err != nil {
        return nil, err
    }

    ret, err := attar.NewSQLiteDB(attar.SQLiteMemoryDSN)
    if err != nil {
        return nil, err
    }

    if _, err = ret.Exec(view.CreateQuery); err != nil {
        return nil, err
    }

    _, err = ret.Exec(view.PopulateQuery)
    return ret, err
}

func (h *ViewHandler) Init(rc enc.CBOR) (attar.HandlerPath, error) {
    view := struct{ EndpointMap *ViewHandler }{h}
    ret, err := attar.HandlerPath{}, rc.Unmarshal(&view)
    if err == nil {
        ret.Path = h.Endpoint
        h.CRUD, err = initDB(rc)
    }
    return ret, err
}

func (h *ViewHandler) Serve(w http.ResponseWriter, r *http.Request) {
    v := reflect.ValueOf(h).MethodByName(publicMethodPrefix + r.Method)
    if v == (reflect.Value{}) {
        attar.InvalidMethod(w, r.Method)
    } else {
        v.Call([]reflect.Value{reflect.ValueOf(w), reflect.ValueOf(r)})
    }
}

func (h *ViewHandler) ReadViewQuerier() attar.Querier {
    const str = "SELECT Route, State FROM view WHERE ID = :id LIMIT 1"
    return attar.NewSQLiteQuerier(h.CRUD.(*attar.SQLiteDB), str)
}

func (h *ViewHandler) PublicREAD(w http.ResponseWriter, r *http.Request) {
    var vid pl.ViewIDPayload
    err := enc.NewSourceCBOR(r.Body).Unmarshal(&vid)
    if err != nil {
        panic(err)
    }

    println("got ID from request:", vid.ID)

    q := h.ReadViewQuerier()
    q.Bind("id", vid.ID)
    if err = q.Query(); err != nil {
        panic(err)
    }

    var ret pl.ViewPayload
    err = q.ReadOne(&ret.Route, &ret.State)
    if err != nil {
        panic(err)
    }

    println("got view from db:", ret.Route, enc.CBOR(ret.State).Hex())

    err = enc.SinkCBOR(w, &ret)
    if err != nil {
        panic(err)
    }
}
