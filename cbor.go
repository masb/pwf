package main

import (
    "pwf/src/common/enc"

    "pwf/demo/routing"
)

/* ----- */

func main() {
    vid := pl.ViewIDPayload{ID: "slt"}
    cbor, err := enc.ToCBOR(&vid)
    if err != nil {
        panic(err)
    }

    println(cbor.Hex(), cbor.String())
}
