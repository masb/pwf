module pwf

go 1.13

require (
	github.com/fxamacker/cbor/v2 v2.2.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	golang.org/x/text v0.3.2
)
